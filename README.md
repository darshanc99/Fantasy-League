# Fantasy League
- - - -
## It's time to be a pro MANAGER
**Bringing in the most played sport in the entire planet at the tip of one’s fingers.**
>Bringing a physical world into a virtual application that can let you create your own fantasy team, and create an environment of self-experiencing how to gain the best players in the squad, and scoring the maximum!!!
- - - -
## What's a Fantasy League?
>FANTASY LEAGUE is a game in which participants (game_users) assemble an imaginary team of real life footballers and score points based on those players' actual statistical performance or their perceived contribution on the field of play.	Usually players are selected from one specific division in a particular country, although there are many variations. 
- - - -
### How do you score a POINT?
Points are then gained or deducted depending on players' performances. 
Points systems vary between games but typically, points are awarded for some or all of the following achievements:-

* If the player **scores** a goal
* If the player **assists** someone on field
* Loses points if the player gets a **Yellow_Card**
* Loses points if the player gets a **Red_Card**
* If the player gets some **saves** for the team
- - - -
### The tools we used
- [x] MySQL Database
- [x] JDBC Driver
- [x] Java Swing class and AWT class majorly for GUI
- - - -
### Contributors
- [@darshanc99].(https://github.com/darshanc99)
- [@nik9hil].(https://github.com/nik9hil)
- [@rohiiit].(https://github.com/rohiiit)
- [@Rajdoshi99].(https://github.com/Rajdoshi99)
- - - -
***ELITE CODERS*** :alien:
